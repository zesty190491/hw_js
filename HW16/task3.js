function timer() {

	let time;

	return function () {
		if (!time) {
			time = Math.round(+new Date() / 1000);
			return 'Enable';
		} else {
			let newTime = Math.round(+new Date() / 1000);
			let diff = newTime - time;
			time = newTime;
			return diff;
		}
	};
}

let getTime = timer();
