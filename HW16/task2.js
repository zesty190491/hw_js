function updateArray() {

	let array = [];

	return function (value) {
		if (!value) {
			array = [];
			return array;
		}
		array.push(value);
		return array;
	};
}

let getUpdatedArr = updateArray();

console.log(getUpdatedArr(3));
console.log(getUpdatedArr(5));
console.log(getUpdatedArr({ name: 'Vasya' }));
console.log(getUpdatedArr());
console.log(getUpdatedArr(4));
