function addListener(type, handler) {
	let resultArr = [];
	const keyNames = type.split('+');
	document.addEventListener('keydown', function (event) {
		event.preventDefault();
		for (let keyName of keyNames) {
			if (event.key === keyName) {
				resultArr.push(event.key);
			}
		}
		if (resultArr.join('+') === type) {
			handler();
			resultArr = [];
		}
	});
	document.addEventListener('keyup', function (event) {
		event.preventDefault();
		for (let keyName of keyNames) {
			if (event.key === keyName) {
				resultArr = [];
			}
		}
	});
}

addListener('Control+p', function () {
	console.log('Hello, world!');
});
