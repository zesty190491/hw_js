const studentsArr = [];

class Student {
	static id = 1;
	constructor(enrollee) {
		this.id = Student.id++;
		this.name = enrollee.name;
		this.surname = enrollee.surname;
		this.ratingPoint = enrollee.ratingPoint
		this.schoolPoint = enrollee.schoolPoint
		this.isSelfPayment = this.calculatePayment();
		studentsArr.push(this);
	}

	calculatePayment() {
		if (this.ratingPoint >= 800) {
			const minRatingPoint = this.getMinRatingPoints();
			if (this.ratingPoint > minRatingPoint) {
				const budgetStudents = this.getBudgetStudents();
				if (budgetStudents.length < 5) {
					return false;
				} else {
					if (this.isRatingPointsGreater(budgetStudents)) {
						const studentWithMinRatingPoints = this.getStudentWithMinRatingPoints(budgetStudents);
						studentWithMinRatingPoints.isSelfPayment = true;
						return false;
					}
					const studentsWithEqualRatingPoints = this.getStudentsWithEqualRatingPoints(budgetStudents);
					for (const student of studentsWithEqualRatingPoints) {
						if (student.schoolPoint < this.schoolPoint) {
							student.isSelfPayment = true;
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	getBudgetStudents() {
		return studentsArr.filter((student) => {
			if (student.isSelfPayment === false) {
				return student;
			}
		});
	}

	getMinRatingPoints() {
		if (studentsArr.length < 1) {
			return 0;
		}
		return Math.min.apply(null, studentsArr.map(student => student.ratingPoint));
	}

	isRatingPointsGreater(budgetStudents) {
		for (let budgetStudent of budgetStudents) {
			if (this.ratingPoint > budgetStudent.ratingPoint) {
				return true;
			}
		}
		return false;
	}

	getStudentsWithEqualRatingPoints(budgetStudents) {
		return budgetStudents.filter((student) => {
			if (student.ratingPoint === this.ratingPoint) {
				return student;
			}
		});
	}

	getStudentWithMinRatingPoints(budgetStudents) {
		budgetStudents.sort((a, b) => {
			return a.ratingPoint - b.ratingPoint;
		});

		return budgetStudents[0];
	}

	static listOfStudents() {
		return studentsArr;
	}
}

const enrolleeArr = [{
	name: 'Сергей',
	surname: 'Войлов',
	ratingPoint: 1000,
	schoolPoint: 1000,
	course: 2,
},
{
	name: 'Татьяна',
	surname: 'Коваленко',
	ratingPoint: 880,
	schoolPoint: 700,
	course: 1,
},
{
	name: 'Анна',
	surname: 'Кугир',
	ratingPoint: 1430,
	schoolPoint: 1200,
	course: 3,
},
{
	name: 'Станислав',
	surname: 'Щелоков',
	ratingPoint: 1130,
	schoolPoint: 1060,
	course: 2,
},
{
	name: 'Денис',
	surname: 'Хрущ',
	ratingPoint: 1000,
	schoolPoint: 990,
	course: 4,
},
{
	name: 'Татьяна',
	surname: 'Капустник',
	ratingPoint: 650,
	schoolPoint: 500,
	course: 3,
},
{
	name: 'Максим',
	surname: 'Меженский',
	ratingPoint: 990,
	schoolPoint: 1100,
	course: 1,
},
{
	name: 'Денис',
	surname: 'Марченко',
	ratingPoint: 570,
	schoolPoint: 1300,
	course: 4,
},
{
	name: 'Антон',
	surname: 'Завадский',
	ratingPoint: 1090,
	schoolPoint: 1010,
	course: 3
},
{
	name: 'Игорь',
	surname: 'Куштым',
	ratingPoint: 870,
	schoolPoint: 790,
	course: 1,
},
{
	name: 'Инна',
	surname: 'Скакунова',
	ratingPoint: 1560,
	schoolPoint: 200,
	course: 2,
},
{
	name: 'Лучший',
	surname: 'Студент',
	ratingPoint: 1560,
	schoolPoint: 300,
	course: 2,
},
{
	name: 'Лучший',
	surname: 'Студент-2',
	ratingPoint: 1560,
	schoolPoint: 350,
	course: 2,
},
{
	name: 'Лучший',
	surname: 'Студент-3',
	ratingPoint: 1560,
	schoolPoint: 351,
	course: 2,
},
{
	name: 'Лучший',
	surname: 'Студент-4',
	ratingPoint: 1560,
	schoolPoint: 400,
	course: 2,
},
{
	name: 'Лучший',
	surname: 'Студент-5',
	ratingPoint: 1560,
	schoolPoint: 150,
	course: 2,
},
{
	name: 'Лучший',
	surname: 'Студент-5',
	ratingPoint: 1560,
	schoolPoint: 400,
	course: 2,
},
];

for (const enrollee of enrolleeArr) {
	new Student(enrollee);
}
