class CustomString {
	strReverse(string) {
		let array = [];
		for (let letter of string) {
			array.unshift(letter);
		}
		return array.join('');
	}

	ucFirst(string) {
		let firstChar = string[0].toUpperCase();
		let elseChars = string.slice(1);
		return firstChar + elseChars;
	}

	ucWords(string) {
		let array = string.split(' ');
		let newArray = [];
		for (let word of array) {
			let ucFirsString = this.ucFirst(word);
			newArray.push(ucFirsString);
		}
		return newArray.join(' ');
	}
}

class Validator {
	checkIsEmail(email) {
		const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	checkIsDomain(domain) {
		const re = /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/;
		return re.test(String(domain));
	}

	checkIsDate(date) {
		const re = /^\d{2}([./-])\d{2}\1\d{4}$/;
		return re.test(String(date));
	}

	checkIsPhone(phone) {
		let rawPhoneNumber = phone.replaceAll(' ', '').replaceAll('-', '').replaceAll('(', '').replaceAll(')', '');
		const re = /^\+380\d{3}\d{2}\d{2}\d{2}$/;
		return re.test(String(phone));
	}
}
